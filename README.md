# verity-mysql-wallet

A MySQL database with a wallet schema. This container is ephemeral and intended for pipeline jobs (intergration tests)

This repo should produce a docker container in this project's registery. Changes to this repo should produce a new container via the CI/CD pipeline.

